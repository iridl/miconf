# README

## Dependencies

* Python 3
* Python module ply

## Build and Install

* Make sure the following libraries are installed, for example:

    * Using yum:

            sudo yum install readline-devel ncurses-devel

    * Using apt-get:

            sudo apt-get install libreadline-dev
            sudo apt-get install libncurses5-dev
	

* Clone miconf repository with flag `--recursive`:

        git clone --recursive git@bitbucket.org:iridl/miconf.git

* Build and install as follows:

        make
        sudo make PREFIX=/usr/local install


## Quick Intro

The main idea behind miconf is providing very simple and sufficiently weird meta markup suitable for almost any programming language or text based format to implement translation units and template substitution.

Translation units feature is implemented by ‘miconf-multi’ and provides two types of translation units: multi-line and in-line:

Example of a multi-line translation unit:

```
. . .
<<<en:
Several words
in English.
|||es:
Algunas
palabras
in español.
|||ru:
Несколько строк
по-русски.
>>>
. . .
```

Example of an in-line translation unit:

```python
. . .
foo = "<<<en:Apple|||ru:Яблоко|||es:Manzana>>>”
. . .
```


Template substitution feature is implemented by ‘miconf-parser' and provides two types of markup: statement “injection” and expression substitution

Example of statement “injection”:

```python
. . .
=== foo = "world"
. . .
```

Example of expression substitution:

```python
. . .
Hello, <<< foo.upper() >>>
. . . 
```

Please note that miconf-multi should be applied before miconf-parser because miconf-parser doesn’t know anything about translation units.

Miconf-multi implements “language fallback”. You may specify multiple languages on the command line, and the utility will use the second language from the list if the translation unit doesnt have the translation for the first choice language (the first language in the list). This feature facilitates gradual translation process. 

We can implement another utility that would analyze the translation units in a git repo and suggest the files (and line numbers) that require attention , i.e. where there are missing and/or outdated translations.  

Currently we use miconf as a configuration tool in Ingrid , Maproom and other builds, and started using it in dL Snippets

Currently we support Python and Lua as a language behind the template. We can easily add other languages

Miconf includes a few other utilities implement batch template processing, i.e. allow to process whole directory trees and preserve chmod flags

Here’s more sophisticated example that includes both translation units and template substitutions and sample run. Please note how we use ‘pass’ statements to set the indentations of the text templates that follow them (this is Python specific):

```python
$ cat a
. . .
=== for x in range(3):
===    for y in range(3):
===       pass
<<< x >>> <<<en:multiply by|||ru:умножить на>>> <<< y >>> = <<< x * y >>>
===    pass
x = <<< x >>>
=== pass
<<<en:
See you later alligator!
|||es:
Chao, pescado!
|||ru:
Пока!
. . .
```

```python
$ miconf-multi a b es,ru,en
$ cat b
. . .
=== for x in range(3):
===    for y in range(3):
===       pass
<<< x >>> умножить на <<< y >>> = <<< x * y >>>
===    pass
x = <<< x >>>
=== pass
Chao, pescado!
. . .
```

```python
$ miconf-parser b c
$ cat c
miconf_output('''. . .''') # 1
miconf_output('\n') # 1
for x in range(3): # 2
  for y in range(3): # 3
     pass # 4
     miconf_output('''''') # 5
     miconf_output(str( x )) # 5
     miconf_output(''' умножить на ''') # 5
     miconf_output(str( y )) # 5
     miconf_output(''' = ''') # 5
     miconf_output(str( x * y )) # 5
     miconf_output('''''') # 5
     miconf_output('\n') # 5
  pass # 6
  miconf_output('''x = ''') # 7
  miconf_output(str( x )) # 7
  miconf_output('''''') # 7
  miconf_output('\n') # 7
pass # 8
miconf_output('''Chao, pescado!''') # 9
miconf_output('\n') # 9
miconf_output('''. . .''') # 10
miconf_output('\n') # 10
```


```bash
$ (echo 'miconf_output=lambda x: print(x,end="")'; cat c) | python >d
$ cat d 
. . .
0 умножить на 0 = 0
0 умножить на 1 = 0
0 умножить на 2 = 0
x = 0
1 умножить на 0 = 0
1 умножить на 1 = 1
1 умножить на 2 = 2
x = 1
2 умножить на 0 = 0
2 умножить на 1 = 2
2 умножить на 2 = 4
x = 2
Chao, pescado!
. . .
```
