/*
 * Copyright (c) 2019 ikh software, inc. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1) Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 2) Redistributions of source code with modification must include a notice
 * that the code was modified.
 * 3) Neither the names of the authors nor the names of their contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>

#include "version.h"

#define OUTFUN  "miconf_output"

#define WRITE_BEG "'''"
#define WRITE_END "'''"

#define NL '\n'
#define SP ' '

#define doNewLine(c) fprintf(fo,"%*s%s('\\n') # %d\n",indent,"",OUTFUN,lineno)

#define begText() fprintf(fo,"%*s%s(%s",indent,"",OUTFUN,WRITE_BEG)
#define doText(c) (c=='\'' ? fprintf(fo,"\\'") : (c=='\\' ? fprintf(fo,"\\\\") :fputc(c,fo)))
#define endText() fprintf(fo,"%s) # %d\n",WRITE_END,lineno)

#define begStat() (indent=0, countIndent=1)
#define doStat(c) fputc(c,fo)
#define endStat() fprintf(fo," # %d\n",lineno)

#define begExpr() fprintf(fo,"%*s%s(str(",indent,"",OUTFUN)
#define doExpr(c) fputc(c,fo)
#define endExpr() fprintf(fo,")) # %d\n", lineno)

void convert(FILE* fi, FILE* fo, int n, int eq, int la, int ra) {
   int lineno = 1;
   int c;
   int s = 1; //state
   int indent = 0;
   int countIndent = 1;
   do {
      c=getc(fi);
      switch (s) {
      case 1:
         switch (c) {
         case EOF: s=1; break;
         case NL: s=1; doNewLine(); break;
         default: 
            if (c==eq) {
               s=2;
            } else if(c==la) {
               s=6; begText();
            } else {
               s=5; begText(); doText(c); 
            }
            break;
         }
         break;
      case 2:
         switch (c) {
         case EOF: s=5; begText(); doText(eq); endText(); break;
         case NL: s=1; begText(); doText(eq); endText(); doNewLine(); break;
         default: 
            if (c==eq) {
               s=3; 
            } else {
               s=5; begText(); doText(eq); doText(c);
            }
            break;
         }
         break;
      case 3:
         switch (c) {
         case EOF: s=5; begText(); doText(eq); doText(eq); endText(); break;
         case NL: s=1; begText(); doText(eq); doText(eq); endText(); doNewLine(); break;
         default: 
            if (c==eq) {
               s=35;
            } else {
               s=5; begText(); doText(eq); doText(eq); doText(c);
            }
            break;
         }
         break;
      case 35:
         switch (c) {
         case EOF: s=5; begText(); doText(eq); doText(eq); doText(eq); endText(); break;
         case NL: s=1; begText(); doText(eq); doText(eq); doText(eq); endText(); doNewLine(); break;
         default:
            if (c==SP) {
               s=4; begStat();
            } else {
               s=5; begText(); doText(eq); doText(eq); doText(eq); doText(c);
            }
            break;
         }
         break;
      case 4:
         switch (c) {
         case EOF: s=1; endStat(); break;
         case NL: s=1; endStat(); break;
         default: 
            s=4; 
            if (countIndent && c==SP) {
               indent ++;
            } else {
               countIndent = 0;
            }
            doStat(c); 
            break;
         }
         break;
      case 5:
         switch (c) {
         case NL: s=1; endText(); doNewLine(); break;
         case EOF: s=5; endText(); break;
         default: 
            if (c==la) {
               s=6;
            } else {
               s=5; doText(c);
            }
            break;
         }
         break;
      case 6:
         switch (c) {
         case EOF: s=5; doText(la); endText(); break;
         case NL: s=1; doText(la); endText(); doNewLine(); break;
         default: 
            if (c==la) {
               s=7; 
            } else {
               s=5; doText(la); doText(c);
            }
            break;
         }
         break;
      case 7:
         switch (c) {
         case EOF: s=5; doText(la); doText(la); endText(); break;
         case NL: s=1; doText(la); doText(la); endText(); doNewLine(); break;
         default: 
            if (c==la) {
               s=8; endText(); begExpr();
            } else {
               s=5; doText(la); doText(la); doText(c);
            }
            break;
         }
         break;
      case 8:
         switch (c) {
         case EOF: s=8; endExpr(); break;
         default: 
            if (c==ra) {
               s=9;
            } else {
               s=8; doExpr(c);
            }
            break;
         }
         break;
      case 9:
         switch (c) {
         case EOF: s=8; doExpr(ra); endExpr(); break;
         default: 
            if (c==ra) {
               s=10; 
            } else {
               s=8; doExpr(ra); doExpr(c);
            }
            break;
         }
         break;
      case 10:
         switch (c) {
         case EOF: s=8; doExpr(ra); doExpr(ra); endExpr(); break;
         default: 
            if (c==ra) {
               s=5; endExpr(); begText();
            } else {
               s=8; doExpr(ra); doExpr(ra); doExpr(c);
            }
            break;
         }
         break;
      } 
      if (c==NL) {
         lineno++;
      }

   } while (c!=EOF);

   if (ferror(fi)) {
      fprintf(stderr,"file read error (errno=%d: %s)\n", errno, strerror(errno));
      exit(1);
   }
}

void process_file(const char* iname, const char* oname, int vflag, int n, int cs, int cle, int cre) {

   if (vflag) {
      fprintf(stderr, "processing file '%s' -> '%s', (%d,'%c','%c','%c')\n", iname, oname, n,cs,cle,cre);
   }

   FILE* fi;
   FILE* fo;
   int isStdin = (strcmp(iname,"-")==0);
   int isStdout = (strcmp(oname,"-")==0);

   if (isStdin) {
      fi = stdin;
   } else {
      fi = fopen(iname, "r");
      if (fi==NULL) {
         fprintf(stderr,"can't open input file (file='%s', errno=%d: %s)\n", iname, errno, strerror(errno));
         exit(1);
      }
   }

   if (isStdout) {
      fo = stdout;
   } else {
      fo = fopen(oname, "w");
      if (fo==NULL) {
         fprintf(stderr,"can't open output file (file='%s', errno=%d: %s)\n", oname, errno, strerror(errno));
         exit(1);
      }
   }

   convert(fi,fo,n,cs,cle,cre);

   if (!isStdin) {
      fclose(fi);
   }
   if (!isStdout) {
      fclose(fo);
   }
}

void version() {
      fprintf(stderr,"Miconf-parser: template parser\n");
      fprintf(stderr,"Miconf-parser %s Copyright (c) 2019 ikh software, inc.\n", miconf_VERSION );
}

void usage() {
      fprintf(stderr,"Miconf-parser: template parser\n");
      fprintf(stderr,"Miconf-parser %s Copyright (c) 2019 ikh software, inc.\n", miconf_VERSION );
      fprintf(stderr,"Commit: %s\n\n", miconf_VERSION_RAW );
      fprintf(stderr,"Usage:\n");
      fprintf(stderr,"   miconf-parser [options] [template_file [output_file]]\n");
      fprintf(stderr,"Options:\n");
      fprintf(stderr,"   -l language -- target language: python, lua\n");
      fprintf(stderr,"   -n markup length (default: 3)\n");
      fprintf(stderr,"   -c three markup chars (default: '=<>')\n");
      fprintf(stderr,"   -v -- verbose\n");
      fprintf(stderr,"   -V -- version\n");
      fprintf(stderr,"   -h -- help\n\n");
}


int main(int argc, char* argv[]) {
   char buf[10000];
   int ch;
   int n = 3;
   char* markup = "=<>";
   int vflag = 0;
   const char* language = "python";

   while ((ch = getopt(argc, argv, "hVvl:n:c:")) != -1) {
      switch (ch) {
      case 'V':
         version();
         exit(0);
      case 'h':
         usage();
         exit(0);
      case 'v': vflag = 1; break;
      case 'n': n = atoi(optarg); break;
      case 'c': markup = optarg; break;
      case 'l': language = optarg; break;
      default:
         usage();
         exit(1);
      }
   }

   const char* iname;
   const char* oname;
   if (argc-optind > 0) {
      iname = argv[optind];
      optind ++;
   } else {
      iname = "-";
   }
   if (argc-optind > 0) {
      oname = argv[optind];
      optind ++;
   } else {
      oname = "-";
   }

   int cs = strlen(markup) < 1 ? '=' : markup[0];
   int cle = strlen(markup) < 2 ? '<' : markup[1];
   int cre = strlen(markup) < 3 ? '>' : markup[2];

   process_file(iname,oname,vflag,n,cs,cle,cre);

   return(0);
}

